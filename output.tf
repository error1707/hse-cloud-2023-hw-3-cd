resource "local_file" "ansible_hosts" {
  content = "[vm]\n${
        yandex_compute_instance.vm.network_interface.0.nat_ip_address
    }\n\n[vm:vars]\nBLUE_PORT=8000\nGREEN_PORT=8001"
  filename = "hosts"
}

output "external_ip_address_nginx" {
  value = yandex_compute_instance.vm.network_interface.0.nat_ip_address
}