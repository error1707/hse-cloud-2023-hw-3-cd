events {
  worker_connections 1024;
}

http {
    upstream logbroker {
        server localhost:{{ PORT_TO_USE }};
    }

    server {
        listen 80;

        location / {
            proxy_pass http://logbroker;
        }
    }
}